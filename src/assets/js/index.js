//Favicon
import '../../favicon.ico';

//Styles
import '../scss/styles.scss';
import '../fonts/font-awesome/css/all.min.css';
import '../css/flickity.css';

//jQuery
import $ from 'jquery';
window.$ = $;

//Add classes to header
const site_header = $("header");
var last_scroll_top = 0;
var st = 0;

$(window).on('scroll', function() {
  st = $(this).scrollTop();

  if (site_header.hasClass("mobile")) {
    return false;
  }

  if(st <= last_scroll_top) {
    //Scroll up
    site_header.removeClass("nav-up");

    //Scroll up, almost in the page top
    if (st <= 100) {
      site_header.removeClass("nav-up").removeClass("scroll-down");
    }
  }
  else {
    if (st >= 100) {
      //Scroll down, hide the header
      site_header.addClass("scroll-down").addClass("nav-up");
    }
  }

  last_scroll_top = st;
});

//Flickity
import Flickity from 'flickity';

var book_slider = new Flickity( '.book-slider', {
  prevNextButtons: false,
  pageDots: false,
  cellAlign: 'left',
  contain: true,
  imagesLoaded: true,
  setGallerySize: true,
  freeScroll: true
});

//Resize for Flickity
$(window).on("load", function () {
  window.dispatchEvent(new Event('resize'));
});

//Set nav cart counter number to 8
$('#cart-counter').text('8');

$(document).ready(function () {

  $(".mobile-nav-trigger").click(function () {
    site_header.toggleClass("mobile");
  });

  //Footer suscribe button
  $(".suscribe-input-button").click(function (e) {
    e.preventDefault();
    //Let's imagine the backend handles this request...
    alert("Llamada al backend para manejar el email");
    //Empty input
    $(".suscribe-input-email").val("");
  });
});
