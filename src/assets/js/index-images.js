//axios
const axios = require('axios');

//Get images
import Logo_Nav from '../img/logo-nav.png';
import Logo_Footer from '../img/logo-footer.png';
import HeroBook from '../img/home/hero-book.png';
import Novedades_Side_Pattern from '../img/pattern-1.jpg';
import Author_1 from '../img/home/author-1.png';
import Dots_1 from '../img/home/dots-1.png';
import Dots_2 from '../img/home/dots-2.png';
import Suscribite_Footer from '../img/suscribite.png';
import Suscribite_Dots from '../img/suscribite-dots.png';
import SocialSnack_Logo from '../img/socialsnack-logo.png';

//Books
import Book_1 from '../img/home/slider/book-1.png';
import Book_2 from '../img/home/slider/book-2.png';
import Book_3 from '../img/home/slider/book-3.png';
import Book_4 from '../img/home/slider/book-4.png';
import Book_5 from '../img/home/slider/book-5.png';
import Book_6 from '../img/home/slider/book-6.png';
import Book_7 from '../img/home/slider/book-7.png';
import Book_8 from '../img/home/slider/book-8.png';

$(document).ready(function () {
  //Header logo
  $('#site-logo').attr('src', Logo_Nav);

  //Hero book
  $('#hero-book').css('background-image', 'url('+HeroBook+')');

  //Books
  $('.book-image-1').attr('src', Book_1);
  $('.book-image-2').attr('src', Book_2);
  $('.book-image-3').attr('src', Book_3);
  $('.book-image-4').attr('src', Book_4);
  $('.book-image-5').attr('src', Book_5);
  $('.book-image-6').attr('src', Book_6);
  $('.book-image-7').attr('src', Book_7);
  $('.book-image-8').attr('src', Book_8);

  //Novedades side pattern
  $('.side-pattern').css('background-image', 'url('+Novedades_Side_Pattern+')');

  //Author photo
  $('.author-image').height($('.author-image').width()).css('background-image', 'url('+Author_1+')');

  //Dots patter
  $('.dots-1').attr('src', Dots_1)
  $('.dots-2').attr('src', Dots_2)

  //Footer logo
  $('#footer-logo').attr('src', Logo_Footer);

  //Newsletter suscribe image
  $('.suscribe-image').attr('src', Suscribite_Footer);

  //Newsletter suscribe dots
  $('.suscribe-dots').attr('src', Suscribite_Dots);

  //SocialSnack logo
  $('#socialsnack-logo').attr('src', SocialSnack_Logo);
});

$(window).on("resize", function () {
  $('.author-image').height($('.author-image').width()).css('background-image', 'url('+Author_1+')');
});


//Get Instagram photos
//Original source: https://codelike.pro/fetch-instagram-posts-from-profile-without-__a-parameter/
async function instagramPosts() {
    // It will contain the photos and post ID
    const response_data = []

    //Custom configuration
    const user_profile = "socialsnack";
    const how_many_posts = 3;

    try {
        const userInfoSource = await axios.get(`https://www.instagram.com/${user_profile}/`)

        // userInfoSource.data contains the HTML from axios
        const jsonObject = userInfoSource.data.match(/<script type="text\/javascript">window\._sharedData = (.*)<\/script>/)[1].slice(0, -1)

        const userInfo = JSON.parse(jsonObject)

        // Retrieve only the first ${how_many_posts} results from the feed
        const mediaArray = userInfo.entry_data.ProfilePage[0].graphql.user.edge_owner_to_timeline_media.edges.splice(0, how_many_posts)
        for (let media of mediaArray) {
            const node = media.node

            // Process only if is an image
            if ((node.__typename && node.__typename !== 'GraphImage')) {
                continue
            }

            // Push the biggest image src into the array
            //node.display_url = 1080x1080 px photo
            response_data.push([
              node.display_url,
              node.edge_media_to_caption.edges[0].node.text,
              node.shortcode //Push the shortcode (the post ID)
            ])
        }
    } catch (e) {
        console.error('Unable to retrieve Instagram photos. Reason: ' + e.toString())
    }

    return response_data
}

(async () => {
  //Get result from instagramPosts()
  var instagram_photos = await instagramPosts()

  //Check if the response array is not empty (ergo, if there was no error in our previous request)
  if (instagram_photos.length != 0) {
    const instagram_photos_parent = $("#instagram-feed-photos");
    $.each( instagram_photos, function( key, value ) {
      const instagram_photo_src = value[0];
      const instagram_photo_text = value[1];
      const instagram_photo_shortcode = value[2];

      instagram_photos_parent.append(`
        <div class="col-xs">
          <a href="https://www.instagram.com/p/${instagram_photo_shortcode}" target="_blank">
            <img src="${instagram_photo_src}" title="${instagram_photo_text}" />
          </a>
        </div>
        `);
      }
    );
  }
})()
